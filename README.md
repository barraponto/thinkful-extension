Small fixes:

* adds toast/sound notifications
* removes unnecessary scroll bars
* fixes ticket height in sidebars
* orders student lists alphabetically

## How to install

Clone this repository.
Go to chrome://extensions then enable developer mode.
Finally, use the `Load Unpacked` option to load the repository as an extension.
