const audio = new Audio('open-ended.ogg');

chrome.runtime.onMessage.addListener(
    ({title, message}, sender, sendResponse) => {
        chrome.notifications.create(
            {
                type: "basic",
                iconUrl: "thinkful.ico",
                title,
                message
            },
            (notificationId) => audio.play()
        );
    }
);
