/* eslint-env browser */
const containerObserver = new MutationObserver((mutations) =>
  mutations.forEach(
    ({ addedNodes }) => {
      addedNodes.forEach(element => {
        [element, ...element.querySelectorAll('.list-view-table-link')].forEach(() => {
          const links = Array.from(document.querySelectorAll('.list-view-table-link'));
          links.sort((a, b) => 
            (a.firstChild.firstChild.textContent < b.firstChild.firstChild.textContent) ? -1 : 1
          )
          links.forEach((element, index) => {
            element.style.order = index+1;
            element.style.backgroundColor = index % 2 ? 'transparent' : '#fafafa';
          })
        })
      })
    })
);

const studentListElement = document.querySelector('.cohort-student-list-container');
containerObserver.observe(studentListElement, { childList: true, subtree: true });
