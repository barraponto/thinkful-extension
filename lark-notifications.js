/* eslint-env browser */
/* global chrome */

const ticketsObserver = new MutationObserver((mutations) =>
    mutations.forEach(({ addedNodes }) =>
        addedNodes.forEach(node => chrome.runtime.sendMessage({
            title: node.querySelector('h6').textContent,
            message: node.querySelector('p').textContent,
        }))
    ));

const ticketListElement = document.querySelector('.teaching-assistant-ticket-list-tickets');
ticketsObserver.observe(ticketListElement, { childList: true });
