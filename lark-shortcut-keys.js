const studentClass = 'teaching-assistant-ticket-link';
const activeStudentClass = 'teaching-assistant-ticket-link__active';
const watchedKeys = e => ['ArrowUp', 'ArrowDown'].includes(e.key);
const validTarget = e => (e.target === document.body) || e.target.classList.contains(activeStudentClass);

document.addEventListener('keydown', e => {
    // if no element has focus, body has it
    if (watchedKeys(e) && validTarget(e)) {
        const activeStudent = document.querySelector(`.${activeStudentClass}`);
        let candidate;
        if (activeStudent) {
            candidate = (e.key === 'ArrowUp') ? activeStudent.previousElementSibling : activeStudent.nextElementSibling;
        } else {
            candidate = document.querySelector(`.${studentClass}`);
        }
        if (candidate) { candidate.click(); candidate.focus() }
      e.preventDefault();
    }
});
